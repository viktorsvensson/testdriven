package org.example;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class CarTest {

    @Mock
    private Engine engine;

    @Test
    public void constructor_withEngine_shouldExist(){
        // Given
        //Engine engine = null;

        // When
        Car car = new Car(engine);

        // Then
        assertNotNull(car);
    }

    @Test
    public void startUp_withGoodOilPressure_shouldFireUp(){

        // Given
        // Engine engine = new StubEngine(50); - stubbe
        when(engine.getOilPressure()).thenReturn(50);
        Car car = new Car(engine);

        // When, Then
        assertDoesNotThrow(car::startUp);
        assertTrue(car.isRunning());
    }

    @Test
    public void startUp_withLowOilPressure_shouldThrowOilPressureException(){
        // Given
        // Engine engine = new StubEngine(10); - stubbe
        when(engine.getOilPressure()).thenReturn(10);
        Car car = new Car(engine);

        // When
        assertThrows(OilPressureException.class, car::startUp);
        verify(engine, times(1)).getOilPressure();
    }


}
