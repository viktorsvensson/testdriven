package org.example;

import org.junit.jupiter.api.Test;


import static org.junit.jupiter.api.Assertions.*;

public class DogTest {

    @Test
    public void constructor_withProperties_shouldExistWithProperties(){

        // Given
        Dog dog;

        // When
        dog = new Dog(12, "Fido");
        int age = dog.getAge();
        String name = dog.getName();

        // Then
        assertAll(
                () -> assertNotNull(dog),
                () -> assertEquals(12, age),
                () -> assertEquals("Fido", name),
                () -> assertEquals(new Dog(12, "Fido"), dog)
        );

    }

}
