package org.example;

public class StubEngine implements Engine {

    private int oilPressure;

    public StubEngine(int oilPressure) {
        this.oilPressure = oilPressure;
    }

    @Override
    public int getOilPressure() {
        return oilPressure;
    }

}
