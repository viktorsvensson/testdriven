package org.example;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.junit.jupiter.params.provider.ValueSource;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

public class UtilsTest {

    /* - utkommenterade för att inte påverka övriga test
    @Test
    @DisplayName("Our first hello world test!")
    public void hello_world_test(){

        // Given
        boolean bool = false;

        // When

        // Then
        assertTrue(bool, "variabeln var inte sann");
    }
     */

    private Utils utils;

    @BeforeEach
    public void setUp(){
        utils = new Utils();
    }

    @Test
    public void divide_withEvenNumbers_shouldDivideEvenly(){

        // Given / Arrange
        int a = 10;
        int b = 5;

        // When / Act
        int result = utils.divide(a, b);

        // Then / Assert
        assertEquals(2, result);
    }

    @Test
    public void divide_dividingByZero_shouldThrowArithmeticException(){
        // Given
        int divident = 5;
        int divisor = 0;

        // When, Then
        ArithmeticException e = assertThrows(ArithmeticException.class, () -> utils.divide(divident, divisor));
        assertEquals("/ by zero", e.getMessage());
    }

    @ParameterizedTest
    @CsvSource(value = {"4, 16", "-4, 16", "0, 0"})
    public void squared_integers_shouldSquaredOriginal(
            int startValue,
            int expected
    ){
        // Given

        // When
        int result = utils.squared(startValue);

        // Then
        assertEquals(expected, result);
    }

    @Test
    public void reverse_withInteger_shouldReverseList(){

        // Given
        List<Integer> numbers = List.of(1, 2, 3);

        // When
        List<Integer> reversedNumbers = utils.reverse(numbers);

        // Then
        assertIterableEquals(List.of(3, 2, 1), reversedNumbers);
        //assertSame(List.of(3, 2, 1), reversedNumbers);

    }


}
