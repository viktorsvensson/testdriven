package org.example;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class DogServiceTest {

    @Mock
    DogRepo dogRepo;

    @Test
    public void findById_withId_shouldReturnCorrectTodo(){

        // Given
        int id = 1;
        when(dogRepo.findById(anyInt())).thenReturn(Optional.of(new Dog(12, "Fido")));
        DogService dogService = new DogService(dogRepo);

        // When
        Dog dog = dogService.findById(id);

        // Then
        verify(dogRepo, times(1)).findById(anyInt());
        assertEquals(new Dog(12, "Fido"), dog);

    }

}
