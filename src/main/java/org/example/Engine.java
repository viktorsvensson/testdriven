package org.example;

public interface Engine {
    int getOilPressure();
}
