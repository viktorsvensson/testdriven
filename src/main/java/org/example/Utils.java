package org.example;

import java.util.Comparator;
import java.util.List;

public class Utils {

    public int divide(int a, int b) {
        //if(b == 0){
        //    throw new ArithmeticException("Hejsan hoppsan");
        //}
        return a / b;
    }

    public List<Integer> reverse(List<Integer> numbers) {

        /*
        List<Integer> revlist = new ArrayList<>();
        for(int i = numbers.size(); i > 0; i--){
            int n = numbers.get(i - 1);
            revlist.add(n);
        }

        return revlist;
        */

        return numbers
                .stream()
                .sorted(Comparator.reverseOrder())
                .toList();
    }

    public int squared(int startValue) {
        return startValue * startValue;
    }
}
