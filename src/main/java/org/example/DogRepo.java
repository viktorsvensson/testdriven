package org.example;

import java.util.Optional;

public interface DogRepo {

    Optional<Dog> findById(int id);

}
