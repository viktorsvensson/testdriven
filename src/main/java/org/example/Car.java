package org.example;

public class Car {
    
    private boolean running;
    private Engine engine;
    
    public Car(Engine engine) {
        this.engine = engine;
    }

    public boolean isRunning() {
        return running;
    }

    public void startUp() {
        if(engine.getOilPressure() < 20){
            throw new OilPressureException();
        }
        running = true;
    }
}
