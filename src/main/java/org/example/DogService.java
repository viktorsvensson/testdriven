package org.example;

public class DogService {

    private final DogRepo dogRepo;

    public DogService(DogRepo dogRepo) {
        this.dogRepo = dogRepo;
    }

    public Dog findById(int id) {
        return dogRepo.findById(id).orElseThrow();
    }

}
